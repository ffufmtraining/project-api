package god.playground.utils

import com.fasterxml.jackson.databind.ObjectMapper
import god.playground.spec.dbo.category.CategoryCategory

object ValidObjectGenerator {
    private var mapper: ObjectMapper = ObjectMapper()

    fun createCategoryCategory(): CategoryCategory {
        return CategoryCategory(
            name = "value"
        )
    }
}