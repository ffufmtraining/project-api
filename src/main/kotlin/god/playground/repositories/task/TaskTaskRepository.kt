package god.playground.repositories.task

import de.ffuf.pass.common.repositories.PassRepository
import god.playground.spec.dbo.project.ProjectProject
import god.playground.spec.dbo.task.TaskTask
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TaskTaskRepository : PassRepository<TaskTask, Long> {
//    @Query(
//        "SELECT t from TaskTask t LEFT JOIN FETCH t.project",
//        countQuery = "SELECT count(id) FROM TaskTask"
//    )
//    fun findAllAndFetchProject(pageable: Pageable): Page<TaskTask>

    @Query(
        "SELECT t from TaskTask t LEFT JOIN FETCH t.project p LEFT JOIN FETCH p.category",
        countQuery = "SELECT count(t.id) FROM TaskTask t"
    )
    fun findAllTasks(pageable: PageRequest): Page<TaskTask>

    @Query(
        "SELECT DISTINCT t from TaskTask t LEFT JOIN FETCH t.project p LEFT JOIN FETCH p.category WHERE t.id IN(:ids)",
        countQuery = "SELECT COUNT(p.id) from ProjectProject p WHERE p.id IN(:ids)"
    )
    fun findFetchAllByIds(ids: List<Long>): List<TaskTask>

    @Query(
        "SELECT t from TaskTask t LEFT JOIN FETCH t.project WHERE t.project.id = :id",
        countQuery = "SELECT count(t.id) FROM TaskTask t WHERE t.project.id = :id"
    )
    fun findByProject(id: Long): List<TaskTask>

    @Query(
        "SELECT t from TaskTask t LEFT JOIN FETCH t.project p LEFT JOIN FETCH p.category WHERE t.project.id = :id",
        countQuery = "SELECT count(t.id) FROM TaskTask t WHERE t.project.id = :id"
    )
    fun findAllTasksByProject(id: Long): List<TaskTask>
}
