package god.playground.repositories.task

import de.ffuf.pass.common.repositories.PassRepository
import god.playground.spec.dbo.task.TaskTaskResource
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TaskTaskResourceRepository : PassRepository<TaskTaskResource, Long> {
//    @Query(
//        "SELECT t from TaskTaskResource t LEFT JOIN FETCH t.task",
//        countQuery = "SELECT count(id) FROM TaskTaskResource"
//    )
//    fun findAllAndFetchTask(pageable: Pageable): Page<TaskTaskResource>
//
//    @Query(
//        "SELECT t from TaskTaskResource t LEFT JOIN FETCH t.resource",
//        countQuery = "SELECT count(id) FROM TaskTaskResource"
//    )
//    fun findAllAndFetchResource(pageable: Pageable): Page<TaskTaskResource>

    @Query(
        "SELECT t from TaskTaskResource t LEFT JOIN FETCH t.task LEFT JOIN FETCH t.resource WHERE t.resource.id = :id",
        countQuery = "SELECT count(t.id) FROM TaskTaskResource t WHERE t.resource.id = :id"
    )
    fun findAllAndFetchByResourceId(id: Long, pageable: Pageable): Page<TaskTaskResource>

    @Query(
        "SELECT t from TaskTaskResource t LEFT JOIN FETCH t.task LEFT JOIN FETCH t.resource WHERE t.task.id = :id",
        countQuery = "SELECT count(t.id) FROM TaskTaskResource t WHERE t.task.id = :id"
    )
    fun findAllAndFetchByTaskId(id: Long, pageable: Pageable): Page<TaskTaskResource>

    @Query(
        "SELECT t from TaskTaskResource t WHERE t.task.id = :id",
        countQuery = "SELECT count(t.id) FROM TaskTaskResource t WHERE t.task.id = :id"
    )
    fun findByTaskId(id: Long): List<TaskTaskResource>

    @Query(
        "SELECT t from TaskTaskResource t WHERE t.resource.id = :id",
        countQuery = "SELECT count(t.id) FROM TaskTaskResource t WHERE t.resource.id = :id"
    )
    fun findByResourceId(id: Long): List<TaskTaskResource>
}
