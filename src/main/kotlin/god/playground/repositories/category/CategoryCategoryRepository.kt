package god.playground.repositories.category

import de.ffuf.pass.common.repositories.PassRepository
import god.playground.spec.dbo.category.CategoryCategory
import org.springframework.data.jpa.repository.Query
import kotlin.Long
import org.springframework.stereotype.Repository

@Repository
interface CategoryCategoryRepository : PassRepository<CategoryCategory, Long> {

    @Query(
        "SELECT DISTINCT c from CategoryCategory c WHERE c.id IN(:ids)",
        countQuery = "SELECT COUNT(c.id) from CategoryCategory c WHERE c.id IN(:ids)"
    )
    fun findFetchAllByIds(ids: List<Long>): List<CategoryCategory>
}
