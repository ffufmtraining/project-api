package god.playground.repositories.project

import de.ffuf.pass.common.repositories.PassRepository
import god.playground.spec.dbo.project.ProjectProject
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ProjectProjectRepository : PassRepository<ProjectProject, Long> {

//    @Query(
//        "SELECT t from ProjectProject t LEFT JOIN FETCH t.category",
//        countQuery = "SELECT count(id) FROM ProjectProject"
//    )
//    fun findAllAndFetchCategory(pageable: Pageable): Page<ProjectProject>

    @Query(
        "SELECT DISTINCT p from ProjectProject p LEFT JOIN FETCH p.category WHERE p.id IN(:ids)",
        countQuery = "SELECT COUNT(p.id) from ProjectProject p WHERE p.id IN(:ids)"
    )
    fun findFetchAllByIds(ids: List<Long>): List<ProjectProject>

    @Query(
        "SELECT p from ProjectProject p LEFT JOIN FETCH p.category WHERE p.category.id = :id",
        countQuery = "SELECT count(p.id) FROM ProjectProject p WHERE p.category.id = :id"
    )
    fun findAllProjectsByCategory(id: Long): List<ProjectProject>

    @Query(
        "SELECT p from ProjectProject p LEFT JOIN FETCH p.category",
        countQuery = "SELECT count(p.id) FROM ProjectProject p"
    )
    fun findAllProjects(pageRequest: PageRequest): Page<ProjectProject>
}
