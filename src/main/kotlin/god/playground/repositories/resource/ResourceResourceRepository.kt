package god.playground.repositories.resource

import de.ffuf.pass.common.repositories.PassRepository
import god.playground.spec.dbo.project.ProjectProject
import god.playground.spec.dbo.resource.ResourceResource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.Query
import kotlin.Long
import org.springframework.stereotype.Repository

@Repository
interface ResourceResourceRepository : PassRepository<ResourceResource, Long> {

    @Query(
        "SELECT r from ResourceResource r WHERE r.id IN(:ids)",
        countQuery = "SELECT COUNT(r.id) from ResourceResource r WHERE r.id IN(:ids)"
    )
    fun findFetchAllByIds(ids: List<Long>): List<ResourceResource>

    @Query(
        "SELECT r from ResourceResource r",
        countQuery = "SELECT count(r.id) FROM ResourceResource r"
    )
    fun findAllProjects(pageable: PageRequest): Page<ResourceResource>
}
