package god.playground.handlerimpl.category

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import de.ffuf.pass.common.utilities.extensions.toPage
import god.playground.repositories.category.CategoryCategoryRepository
import god.playground.spec.dbo.category.CategoryCategory
import god.playground.spec.dbo.category.CategoryCategoryDTO
import god.playground.spec.handler.category.CategoryCategoryDatabaseHandler
import kotlin.Int
import kotlin.Long
import kotlin.collections.List
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("category.CategoryCategoryHandler")
class CategoryCategoryHandlerImpl : PassDatabaseHandler<CategoryCategory,
        CategoryCategoryRepository>(), CategoryCategoryDatabaseHandler {
    /**
     * Get Categories by ids: Returns a list of Categories objects filterd by the submitted ids.
     * HTTP Code 201: The filtred list of Categories objects
     */
    override suspend fun getByIds(body: List<Long>): Page<CategoryCategoryDTO> {
        val categories = repository.findFetchAllByIds(body)
        return categories.toDtos().toPage()
    }

    /**
     * Deletes all Categories: Deletes all Categories and their children.
     * HTTP Code 200: Deltion was successfully.
     */
    override suspend fun removeAll() {
        return repository.deleteAll()
    }

    /**
     * Get all Categories: Returns all Categories from the system that the user has access to.
     * HTTP Code 200: List of Categories
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<CategoryCategoryDTO> {
        val pageable = PageRequest.of(page, maxResults)
        return repository.findAll(pageable).toDtos()
    }

    /**
     * Create Category: Creates a new Category object
     * HTTP Code 201: The created Category
     */
    override suspend fun create(body: CategoryCategoryDTO): CategoryCategoryDTO {
        return repository.save(body.toEntity()).toDto()
    }

    /**
     * Delete Category by id.: Deletes one specific Category.
     */
    override suspend fun remove(id: Long) {
        val category = repository.findById(id).orElseThrow404(id)
        return repository.delete(category)
    }

    /**
     * Finds Categories by ID: Returns Categories based on ID
     * HTTP Code 200: The Category object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): CategoryCategoryDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Update the Category: Updates an existing Category
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: CategoryCategoryDTO, id: Long): CategoryCategoryDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original.copy(
            name = body.toEntity().name,
        )).toDto()
    }
}
