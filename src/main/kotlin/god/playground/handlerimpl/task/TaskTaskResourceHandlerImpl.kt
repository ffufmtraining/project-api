package god.playground.handlerimpl.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import god.playground.repositories.project.ProjectProjectRepository
import god.playground.repositories.resource.ResourceResourceRepository
import god.playground.repositories.task.TaskTaskRepository
import god.playground.repositories.task.TaskTaskResourceRepository
import god.playground.spec.dbo.task.TaskTaskResource
import god.playground.spec.dbo.task.TaskTaskResourceDTO
import god.playground.spec.handler.task.TaskTaskResourceDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("task.TaskTaskResourceHandler")
class TaskTaskResourceHandlerImpl(
    private val taskRepository: TaskTaskRepository,
    private val resourceRepository: ResourceResourceRepository,
) : PassDatabaseHandler<TaskTaskResource,
        TaskTaskResourceRepository>(), TaskTaskResourceDatabaseHandler {
    /**
     * Deletes all object of the parent Resource: Deletes TaskResources objects by the parent
     * Resource id
     * HTTP Code 200: Successfully delted all child objects
     */
    override suspend fun removeByResource(id: Long) {
        val original = repository.findByResourceId(id)
        return repository.deleteAll(original)
    }

    /**
     * Find by Resource: Finds TaskResources by the parent Resource id
     * HTTP Code 200: List of TaskResources items
     */
    override suspend fun getByResource(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<TaskTaskResourceDTO> {
        val pageable = PageRequest.of(page, maxResults)
        return repository.findAllAndFetchByResourceId(id, pageable).toDtos()
    }

    /**
     * Create TaskResource: Creates a new TaskResource object
     * HTTP Code 201: The created TaskResource
     */
    override suspend fun create(body: TaskTaskResourceDTO) : TaskTaskResourceDTO {
        val task = taskRepository.findById(body.toEntity().task?.id!!).orElseThrow404(body.toEntity().task?.id!!)
        val resource = resourceRepository.findById(body.toEntity().resource?.id!!).orElseThrow404(body.toEntity().resource?.id!!)

        return repository.save(body.toEntity().copy(
            task = task,
            resource = resource
        )).toDto()
    }

    /**
     * Deletes all object of the parent Task: Deletes TaskResources objects by the parent Task id
     * HTTP Code 200: Successfully delted all child objects
     */
    override suspend fun removeByTask(id: Long) {
        val original = repository.findByTaskId(id)
        return repository.deleteAll(original)
    }

    /**
     * Find by Task: Finds TaskResources by the parent Task id
     * HTTP Code 200: List of TaskResources items
     */
    override suspend fun getByTask(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<TaskTaskResourceDTO> {
        val pageable = PageRequest.of(page, maxResults)
        return repository.findAllAndFetchByTaskId(id, pageable).toDtos()
    }
}
