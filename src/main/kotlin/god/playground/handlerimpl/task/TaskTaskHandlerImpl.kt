package god.playground.handlerimpl.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import de.ffuf.pass.common.utilities.extensions.toPage
import god.playground.repositories.category.CategoryCategoryRepository
import god.playground.repositories.project.ProjectProjectRepository
import god.playground.repositories.task.TaskTaskRepository
import god.playground.spec.dbo.task.TaskTask
import god.playground.spec.dbo.task.TaskTaskDTO
import god.playground.spec.handler.task.TaskTaskDatabaseHandler
import kotlin.Int
import kotlin.Long
import kotlin.collections.List
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component("task.TaskTaskHandler")
class TaskTaskHandlerImpl(
    private val projectRepository: ProjectProjectRepository,
    private val categoryRepository: CategoryCategoryRepository,
) : PassDatabaseHandler<TaskTask, TaskTaskRepository>(),
        TaskTaskDatabaseHandler {
    /**
     * Get Tasks by ids: Returns a list of Tasks objects filterd by the submitted ids.
     * HTTP Code 201: The filtred list of Tasks objects
     */
    override suspend fun getByIds(body: List<Long>): Page<TaskTaskDTO> {
        val tasks = repository.findFetchAllByIds(body)
        return tasks.toDtos().toPage()
    }

    /**
     * Deletes all object of the parent Project: Deletes Tasks objects by the parent Project id
     * HTTP Code 200: Successfully delted all child objects
     */
    override suspend fun removeByProject(id: Long) {
        val tasks = repository.findByProject(id)
        return repository.deleteAll(tasks)
    }

    /**
     * Find by Project: Finds Tasks by the parent Project id
     * HTTP Code 200: List of Tasks items
     */
    override suspend fun getByProject(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<TaskTaskDTO> {
        val tasks = repository.findAllTasksByProject(id)
        return tasks.toDtos().toPage()
    }

    /**
     * Deletes all Tasks: Deletes all Tasks and their children.
     * HTTP Code 200: Deltion was successfully.
     */
    override suspend fun removeAll() {
        return repository.deleteAll()
    }

    /**
     * Get all Tasks by page: Returns all Tasks from the system that the user has access to. The
     * Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with Pagination.
     * HTTP Code 200: List of Tasks
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<TaskTaskDTO> {
        val pageable = PageRequest.of(page, maxResults)
        return repository.findAllTasks(pageable).toDtos()
    }

    /**
     * Create Task: Creates a new Task object
     * HTTP Code 201: The created Task
     */
    override suspend fun create(body: TaskTaskDTO): TaskTaskDTO {
        val project = projectRepository.findById(body.toEntity().project?.id!!).orElseThrow404(1)
        val category = categoryRepository.findById(project?.category?.id!!).orElseThrow404(1)
        body.validateOrThrow()

        return repository.save(body.toEntity().copy(
            name = body.toEntity().name,
            description = body.toEntity().description,
            project = project.copy(
                category = category
            )
        )).toDto()
    }

    /**
     * Delete Task by id.: Deletes one specific Task.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Finds Tasks by ID: Returns Tasks based on ID
     * HTTP Code 200: The Task object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): TaskTaskDTO? {
        val task = repository.findById(id).orElseThrow404(id)
        val project = projectRepository.findById(task.project?.id!!).orElseThrow404(task.project?.id!!)
        val category = categoryRepository.findById(project.category?.id!!).orElseThrow404(project.category?.id!!)
        return task.copy(
            project = project.copy(
                category = category
            )
        ).toDto()
    }

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO {
        val task = repository.findById(id).orElseThrow404(id)
        val project = projectRepository.findById(body.toEntity().project?.id!!).orElseThrow404(id)
        val category = categoryRepository.findById(project.category?.id!!).orElseThrow404(id)
        return repository.save(task.copy(
            name = body.toEntity().name,
            description = body.toEntity().description,
            project = project.copy(
                category = category
            )
        )).toDto()
    }
}
