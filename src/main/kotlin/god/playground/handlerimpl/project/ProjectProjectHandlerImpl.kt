package god.playground.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import de.ffuf.pass.common.utilities.extensions.toPage
import god.playground.repositories.category.CategoryCategoryRepository
import god.playground.repositories.project.ProjectProjectRepository
import god.playground.spec.dbo.project.ProjectProject
import god.playground.spec.dbo.project.ProjectProjectDTO
import god.playground.spec.handler.project.ProjectProjectDatabaseHandler
import kotlin.Int
import kotlin.Long
import kotlin.collections.List
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl(
    private val categoryRepository: CategoryCategoryRepository,
) : PassDatabaseHandler<ProjectProject, ProjectProjectRepository>(),
        ProjectProjectDatabaseHandler {
    /**
     * Get Projects by ids: Returns a list of Projects objects filterd by the submitted ids.
     * HTTP Code 201: The filtred list of Projects objects
     */
    override suspend fun getByIds(body: List<Long>): Page<ProjectProjectDTO> {
        val projects = repository.findFetchAllByIds(body)
        return projects.toDtos().toPage()
    }

    /**
     * Deletes all object of the parent Category: Deletes Projects objects by the parent Category id
     * HTTP Code 200: Successfully delted all child objects
     */
    override suspend fun removeByCategory(id: Long) {
        val projects = repository.findAllProjectsByCategory(id)
        return repository.deleteAll(projects)
    }

    /**
     * Find by Category: Finds Projects by the parent Category id
     * HTTP Code 200: List of Projects items
     */
    override suspend fun getByCategory(
        id: Long,
        maxResults: Int,
        page: Int
    ): Page<ProjectProjectDTO> {
        val projects = repository.findAllProjectsByCategory(id)
        return projects.toDtos().toPage()
    }

    /**
     * Deletes all Projects: Deletes all Projects and their children.
     * HTTP Code 200: Deltion was successfully.
     */
    override suspend fun removeAll() {
        return repository.deleteAll()
    }

    /**
     * Get all Projects: Returns all Projects from the system that the user has access to.
     * HTTP Code 200: List of Projects
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        val pageable = PageRequest.of(page, maxResults)
        return repository.findAllProjects(pageable).toDtos()
    }

    /**
     * Finds Projects by ID: Returns Projects based on ID
     * HTTP Code 200: The Project object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): ProjectProjectDTO? {
        val project = repository.findById(id).orElseThrow404(id)
        val category = categoryRepository.findById(project.category?.id!!).orElseThrow404(project.category?.id!!)
        return project.copy(
            category = category
        ).toDto()
    }

    /**
     * Create Project: Creates a new Project object
     * HTTP Code 201: The created Project
     */
    override suspend fun create(body: ProjectProjectDTO): ProjectProjectDTO {
        val category = categoryRepository.findById(body.toEntity().category?.id!!).orElseThrow404(1)
        body.validateOrThrow()

        return repository.save(body.toEntity().copy(
            createdAt = OffsetDateTime.now(),
            category = category
        )).toDto()
    }

    /**
     * Delete Project by id.: Deletes one specific Project.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val project = repository.findById(id).orElseThrow404(id)
        val category = categoryRepository.findById(body.category?.id!!).orElseThrow404(id)
        return repository.save(project.copy(
            name = body.toEntity().name,
            description = body.toEntity().description,
            category = category
        )).toDto()
    }
}
