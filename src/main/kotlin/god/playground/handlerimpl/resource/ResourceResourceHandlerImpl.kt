package god.playground.handlerimpl.resource

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import de.ffuf.pass.common.utilities.extensions.toPage
import god.playground.repositories.resource.ResourceResourceRepository
import god.playground.spec.dbo.resource.ResourceResource
import god.playground.spec.dbo.resource.ResourceResourceDTO
import god.playground.spec.handler.resource.ResourceResourceDatabaseHandler
import kotlin.Long
import kotlin.collections.List
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component

@Component("resource.ResourceResourceHandler")
class ResourceResourceHandlerImpl : PassDatabaseHandler<ResourceResource,
        ResourceResourceRepository>(), ResourceResourceDatabaseHandler {
    /**
     * Get Resources by ids: Returns a list of Resources objects filterd by the submitted ids.
     * HTTP Code 201: The filtred list of Resources objects
     */
    override suspend fun getByIds(body: List<Long>): Page<ResourceResourceDTO> {
        val resources = repository.findFetchAllByIds(body)
        return resources.toDtos().toPage()
    }

    /**
     * Get all Resources: Returns all Resources from the system that the user has access to.
     * HTTP Code 200: List of Resources
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<ResourceResourceDTO> {
        val pageable = PageRequest.of(page, maxResults)
        return repository.findAllProjects(pageable).toDtos()
    }

    /**
     * Finds Resources by ID: Returns Resources based on ID
     * HTTP Code 200: The Resource object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): ResourceResourceDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Deletes all Resources: Deletes all Resources and their children.
     * HTTP Code 200: Deltion was successfully.
     */
    override suspend fun removeAll() {
        return repository.deleteAll()
    }

    /**
     * Create Resource: Creates a new Resource object
     * HTTP Code 201: The created Resource
     */
    override suspend fun create(body: ResourceResourceDTO): ResourceResourceDTO {
        return repository.save(body.toEntity()).toDto()
    }

    /**
     * Delete Resource by id.: Deletes one specific Resource.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * Update the Resource: Updates an existing Resource
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: ResourceResourceDTO, id: Long): ResourceResourceDTO {
        val resource = repository.findById(id).orElseThrow404(id)
        return repository.save(resource.copy(
            firstName = body.toEntity().firstName,
            lastName = body.toEntity().lastName,
            position = body.toEntity().position
        )).toDto()
    }
}
