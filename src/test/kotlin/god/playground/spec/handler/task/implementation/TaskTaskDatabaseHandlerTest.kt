package god.playground.spec.handler.task.implementation

import god.playground.PassTestBase
import god.playground.repositories.task.TaskTaskRepository
import god.playground.spec.dbo.task.TaskTask
import god.playground.spec.handler.task.TaskTaskDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class TaskTaskDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var taskTaskRepository: TaskTaskRepository

    @Autowired
    lateinit var taskTaskDatabaseHandler: TaskTaskDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        taskTaskRepository.deleteAll()
    }

//    @Test
//    fun `test getByIds`() = runBlocking {
//        val body: List<Long> = listOf()
//        taskTaskDatabaseHandler.getByIds(body)
//        Unit
//    }
//
//    @Test
//    fun `test removeByProject`() = runBlocking {
//        val id: Long = 0
//        taskTaskDatabaseHandler.removeByProject(id)
//        Unit
//    }
//
//    @Test
//    fun `test getByProject`() = runBlocking {
//        val id: Long = 0
//        val maxResults: Int = 100
//        val page: Int = 0
//        taskTaskDatabaseHandler.getByProject(id, maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test removeAll`() = runBlocking {
//        taskTaskDatabaseHandler.removeAll()
//        Unit
//    }
//
//    @Test
//    fun `test getAll`() = runBlocking {
//        val maxResults: Int = 100
//        val page: Int = 0
//        taskTaskDatabaseHandler.getAll(maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test create`() = runBlocking {
//        val body: TaskTask = TaskTask()
//        taskTaskDatabaseHandler.create(body)
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        taskTaskDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        taskTaskDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: TaskTask = TaskTask()
//        val id: Long = 0
//        taskTaskDatabaseHandler.update(body, id)
//        Unit
//    }
}
