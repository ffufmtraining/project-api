package god.playground.spec.handler.task.integration

import com.fasterxml.jackson.databind.ObjectMapper
import god.playground.PassTestBase
import god.playground.repositories.task.TaskTaskResourceRepository
import god.playground.spec.dbo.task.TaskTaskResource
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class TaskTaskResourceHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var taskTaskResourceRepository: TaskTaskResourceRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        taskTaskResourceRepository.deleteAll()
    }

//    @Test
//    @WithMockUser
//    fun `test removeByResource`() {
//        val id: Long = 0
//                mockMvc.delete("/resources/{id}/task-resources/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getByResource`() {
//        val id: Long = 0
//                mockMvc.get("/resources/{id}/task-resources/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test create`() {
//        val body: TaskTaskResource = TaskTaskResource()
//                mockMvc.post("/task-resources/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test removeByTask`() {
//        val id: Long = 0
//                mockMvc.delete("/tasks/{id}/task-resources/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getByTask`() {
//        val id: Long = 0
//                mockMvc.get("/tasks/{id}/task-resources/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
}
