package god.playground.spec.handler.task.implementation

import god.playground.PassTestBase
import god.playground.repositories.task.TaskTaskResourceRepository
import god.playground.spec.dbo.task.TaskTaskResource
import god.playground.spec.handler.task.TaskTaskResourceDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class TaskTaskResourceDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var taskTaskResourceRepository: TaskTaskResourceRepository

    @Autowired
    lateinit var taskTaskResourceDatabaseHandler: TaskTaskResourceDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        taskTaskResourceRepository.deleteAll()
    }

//    @Test
//    fun `test removeByResource`() = runBlocking {
//        val id: Long = 0
//        taskTaskResourceDatabaseHandler.removeByResource(id)
//        Unit
//    }
//
//    @Test
//    fun `test getByResource`() = runBlocking {
//        val id: Long = 0
//        val maxResults: Int = 100
//        val page: Int = 0
//        taskTaskResourceDatabaseHandler.getByResource(id, maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test create`() = runBlocking {
//        val body: TaskTaskResource = TaskTaskResource()
//        taskTaskResourceDatabaseHandler.create(body)
//        Unit
//    }
//
//    @Test
//    fun `test removeByTask`() = runBlocking {
//        val id: Long = 0
//        taskTaskResourceDatabaseHandler.removeByTask(id)
//        Unit
//    }
//
//    @Test
//    fun `test getByTask`() = runBlocking {
//        val id: Long = 0
//        val maxResults: Int = 100
//        val page: Int = 0
//        taskTaskResourceDatabaseHandler.getByTask(id, maxResults, page)
//        Unit
//    }
}
