package god.playground.spec.handler.task.integration

import com.fasterxml.jackson.databind.ObjectMapper
import god.playground.PassTestBase
import god.playground.repositories.task.TaskTaskRepository
import god.playground.spec.dbo.task.TaskTask
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class TaskTaskHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var taskTaskRepository: TaskTaskRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        taskTaskRepository.deleteAll()
    }

//    @Test
//    @WithMockUser
//    fun `test getByIds`() {
//        val body: List<Long> = listOf()
//                mockMvc.post("/batch-selects/tasks/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test removeByProject`() {
//        val id: Long = 0
//                mockMvc.delete("/projects/{id}/tasks/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getByProject`() {
//        val id: Long = 0
//                mockMvc.get("/projects/{id}/tasks/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test removeAll`() {
//                mockMvc.delete("/tasks/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getAll`() {
//                mockMvc.get("/tasks/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test create`() {
//        val body: TaskTask = TaskTask()
//                mockMvc.post("/tasks/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/tasks/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/tasks/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test update`() {
//        val body: TaskTask = TaskTask()
//        val id: Long = 0
//                mockMvc.put("/tasks/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
}
