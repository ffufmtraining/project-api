package god.playground.spec.handler.project.implementation

import god.playground.PassTestBase
import god.playground.repositories.project.ProjectProjectRepository
import god.playground.spec.dbo.project.ProjectProject
import god.playground.spec.handler.project.ProjectProjectDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class ProjectProjectDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        projectProjectRepository.deleteAll()
    }

//    @Test
//    fun `test getByIds`() = runBlocking {
//        val body: List<Long> = listOf()
//        projectProjectDatabaseHandler.getByIds(body)
//        Unit
//    }
//
//    @Test
//    fun `test removeByCategory`() = runBlocking {
//        val id: Long = 0
//        projectProjectDatabaseHandler.removeByCategory(id)
//        Unit
//    }
//
//    @Test
//    fun `test getByCategory`() = runBlocking {
//        val id: Long = 0
//        val maxResults: Int = 100
//        val page: Int = 0
//        projectProjectDatabaseHandler.getByCategory(id, maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test removeAll`() = runBlocking {
//        projectProjectDatabaseHandler.removeAll()
//        Unit
//    }
//
//    @Test
//    fun `test getAll`() = runBlocking {
//        val maxResults: Int = 100
//        val page: Int = 0
//        projectProjectDatabaseHandler.getAll(maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test create`() = runBlocking {
//        val body: ProjectProject = ProjectProject()
//        projectProjectDatabaseHandler.create(body)
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        projectProjectDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        projectProjectDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: ProjectProject = ProjectProject()
//        val id: Long = 0
//        projectProjectDatabaseHandler.update(body, id)
//        Unit
//    }
}
