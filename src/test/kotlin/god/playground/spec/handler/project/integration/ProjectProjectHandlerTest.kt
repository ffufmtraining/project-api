package god.playground.spec.handler.project.integration

import com.fasterxml.jackson.databind.ObjectMapper
import god.playground.PassTestBase
import god.playground.repositories.project.ProjectProjectRepository
import god.playground.spec.dbo.project.ProjectProject
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class ProjectProjectHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        projectProjectRepository.deleteAll()
    }

    @Test
    @WithMockUser
    fun `test getByIds`() {
        val body: List<Long> = listOf()
                mockMvc.post("/batch-selects/projects/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test removeByCategory`() {
        val id: Long = 0
                mockMvc.delete("/categories/{id}/projects/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test getByCategory`() {
        val id: Long = 0
                mockMvc.get("/categories/{id}/projects/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test removeAll`() {
                mockMvc.delete("/projects/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test getAll`() {
                mockMvc.get("/projects/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test create`() {
        val body: ProjectProject = ProjectProject()
                mockMvc.post("/projects/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test remove`() {
        val id: Long = 0
                mockMvc.delete("/projects/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test getById`() {
        val id: Long = 0
                mockMvc.get("/projects/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test update`() {
        val body: ProjectProject = ProjectProject()
        val id: Long = 0
                mockMvc.put("/projects/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                    
                }
    }
}
