package god.playground.spec.handler.category.implementation

import god.playground.PassTestBase
import god.playground.repositories.category.CategoryCategoryRepository
import god.playground.spec.handler.category.CategoryCategoryDatabaseHandler
import god.playground.utils.ValidObjectGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class CategoryCategoryDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var categoryCategoryRepository: CategoryCategoryRepository

    @Autowired
    lateinit var categoryCategoryDatabaseHandler: CategoryCategoryDatabaseHandler

    @Before
    fun setUp() = runBlocking {
        repeat(5) {
            categoryCategoryRepository.save(ValidObjectGenerator.createCategoryCategory())
        }
    }

    @After
    fun cleanRepositories() {
        categoryCategoryRepository.deleteAll()
    }

    @Test
    fun `test getAll`() = runBlocking {
        repeat(5) {
            categoryCategoryRepository.save(ValidObjectGenerator.createCategoryCategory())
        }
        val page = 1
        val maxResults = 5
        val expectedCategory = categoryCategoryDatabaseHandler.getAll(maxResults, page)
        assertEquals(maxResults, expectedCategory.content.size)
        assertEquals(page, expectedCategory.pageable.pageNumber)
    }

    @Test
    fun `test create`() = runBlocking {
        val body = ValidObjectGenerator.createCategoryCategory()
        val actualCategory = categoryCategoryDatabaseHandler.create(body.toDto())
        val expectedCategory = categoryCategoryRepository.findAll().last()

        assertEquals(6, categoryCategoryRepository.count())
        assertEquals(actualCategory.id, expectedCategory.id)
    }

    @Test
    fun `test getById`() = runBlocking {
        val body = ValidObjectGenerator.createCategoryCategory()
        val actualCategory = categoryCategoryDatabaseHandler.create(body.toDto())
        val expectedCategory = categoryCategoryDatabaseHandler.getById(actualCategory.id!!)!!

        assertEquals(actualCategory.id, expectedCategory.id)
    }

    @Test
    fun `test getByIds`() = runBlocking {
        val body: List<Long> = listOf(1, 2)
        val actualCategories = categoryCategoryDatabaseHandler.getByIds(body)
        assertEquals(2,actualCategories.size)
    }

    @Test
    fun `test update`() = runBlocking {
        val category = categoryCategoryDatabaseHandler.create(
            ValidObjectGenerator.createCategoryCategory().toDto()
        )

        categoryCategoryDatabaseHandler.update(
            category.copy(
                name = "new name"
            ),
            category.id!!
        )

        val updatedCategory = categoryCategoryRepository.findById(category.id!!).get()
        assertEquals("new name", updatedCategory.name)
    }

    @Test
    fun `test remove`() = runBlocking {
        val category = categoryCategoryRepository.save(
            ValidObjectGenerator.createCategoryCategory()
        )
        assertEquals(true, categoryCategoryRepository.existsById(category.id!!))

        categoryCategoryDatabaseHandler.remove(category.id!!)
        assertEquals(false, categoryCategoryRepository.existsById(category.id!!))
    }

//    @Test
//    fun `test getByIds`() = runBlocking {
//        val body: List<Long> = listOf()
//        categoryCategoryDatabaseHandler.getByIds(body)
//        Unit
//    }
//
//    @Test
//    fun `test removeAll`() = runBlocking {
//        categoryCategoryDatabaseHandler.removeAll()
//        Unit
//    }
//
//    @Test
//    fun `test getAll`() = runBlocking {
//        val maxResults: Int = 100
//        val page: Int = 0
//        categoryCategoryDatabaseHandler.getAll(maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test create`() = runBlocking {
//        val body: CategoryCategory = CategoryCategory()
//        categoryCategoryDatabaseHandler.create(body)
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        categoryCategoryDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        categoryCategoryDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: CategoryCategory = CategoryCategory()
//        val id: Long = 0
//        categoryCategoryDatabaseHandler.update(body, id)
//        Unit
//    }
}
