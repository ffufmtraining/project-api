package god.playground.spec.handler.category.validation

import god.playground.PassTestBase
import god.playground.utils.ValidObjectGenerator
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.jupiter.api.assertDoesNotThrow

class CategoryCategoryValidatorTest: PassTestBase() {

    private val category = ValidObjectGenerator.createCategoryCategory()

    @Test
    fun `test valid body`() {
        val valid = ValidObjectGenerator.createCategoryCategory()

        assertDoesNotThrow {
            valid.validateOrThrow()
        }
    }

    @Test
    fun `test invalid body`() {
        val exception = Assertions.assertThatThrownBy {
            category.copy(
                name = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
                        "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco " +
                        "laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in " +
                        "voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat " +
                        "cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ).validateOrThrow()
        }
        exception.hasMessage("""422 UNPROCESSABLE_ENTITY "[{"message":"The size of \"name\" must be less than or equal 
            |to 250. The given size is 445","field":"name"}]"""".trimMargin())
    }
}