package god.playground.spec.handler.category.integration

import com.fasterxml.jackson.databind.ObjectMapper
import god.playground.PassTestBase
import god.playground.repositories.category.CategoryCategoryRepository
import god.playground.spec.dbo.category.CategoryCategory
import god.playground.utils.ValidObjectGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class CategoryCategoryHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var categoryCategoryRepository: CategoryCategoryRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    fun setUp() {
        repeat(2) {
            categoryCategoryRepository.save(ValidObjectGenerator.createCategoryCategory())
        }
    }

    @After
    fun cleanRepositories() {
        categoryCategoryRepository.deleteAll()
    }

    @Test
    fun `test getAll`() {
        val maxResults = 10
        val page = 1
        repeat(10) {
            categoryCategoryRepository.save(
                ValidObjectGenerator.createCategoryCategory()
            )
        }
        mockMvc.get("/categories/?page=$page&maxResults=$maxResults") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
            header {
                string("TotalElements", "12")
            }
            header {
                string("TotalPages", "2")
            }
            header {
                string("CurrentPage", "1")
            }
            header {
                string("PerPage", "2")
            }
            jsonPath("$.length()") { value(2) }
        }
    }

    @Test
    fun `test create`() {
        val body: CategoryCategory = ValidObjectGenerator.createCategoryCategory()
        mockMvc.post("/categories/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
            jsonPath("$.name") { value(body.name) }
        }
    }

    @Test
    fun `test getById`() {
        val category = categoryCategoryRepository.save(ValidObjectGenerator.createCategoryCategory())
        val id = category.id!!
        mockMvc.get("/categories/$id/") {
            accept(MediaType.APPLICATION_JSON)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `test getByIds`() {
        val body: List<Long> = listOf(1)
        mockMvc.post("/batch-selects/categories//") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
            jsonPath("$.length()") { value(1) }
        }
    }

    @Test
    fun `test update`() {
        val category = categoryCategoryRepository.findAll().first()
        val id = category.id!!
        val body: CategoryCategory = category.copy(
            name = "new name"
        )
        mockMvc.put("/categories/{id}/", id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
            jsonPath("$.name") { value(body.name) }
        }
    }

    @Test
    fun `test remove`() {
        val category = categoryCategoryRepository.save(ValidObjectGenerator.createCategoryCategory())
        val id = category.id!!
        mockMvc.delete("/categories/$id/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `test removeAll`() {
        mockMvc.delete("/categories/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

//    @Test
//    @WithMockUser
//    fun `test getByIds`() {
//        val body: List<Long> = listOf()
//                mockMvc.post("/batch-selects/categories/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test removeAll`() {
//                mockMvc.delete("/categories/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getAll`() {
//                mockMvc.get("/categories/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test create`() {
//        val body: CategoryCategory = CategoryCategory()
//                mockMvc.post("/categories/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/categories/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/categories/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test update`() {
//        val body: CategoryCategory = CategoryCategory()
//        val id: Long = 0
//                mockMvc.put("/categories/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
}
