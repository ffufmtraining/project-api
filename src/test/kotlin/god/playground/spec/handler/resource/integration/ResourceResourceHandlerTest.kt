package god.playground.spec.handler.resource.integration

import com.fasterxml.jackson.databind.ObjectMapper
import god.playground.PassTestBase
import god.playground.repositories.resource.ResourceResourceRepository
import god.playground.spec.dbo.resource.ResourceResource
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class ResourceResourceHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var resourceResourceRepository: ResourceResourceRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    @After
    fun cleanRepositories() {
        resourceResourceRepository.deleteAll()
    }

//    @Test
//    @WithMockUser
//    fun `test getByIds`() {
//        val body: List<Long> = listOf()
//                mockMvc.post("/batch-selects/resources/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test removeAll`() {
//                mockMvc.delete("/resources/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getAll`() {
//                mockMvc.get("/resources/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test create`() {
//        val body: ResourceResource = ResourceResource()
//                mockMvc.post("/resources/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/resources/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/resources/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test update`() {
//        val body: ResourceResource = ResourceResource()
//        val id: Long = 0
//                mockMvc.put("/resources/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk() }
//
//                }
//    }
}
