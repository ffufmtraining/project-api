package god.playground.spec.handler.resource.implementation

import god.playground.PassTestBase
import god.playground.repositories.resource.ResourceResourceRepository
import god.playground.spec.dbo.resource.ResourceResource
import god.playground.spec.handler.resource.ResourceResourceDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class ResourceResourceDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var resourceResourceRepository: ResourceResourceRepository

    @Autowired
    lateinit var resourceResourceDatabaseHandler: ResourceResourceDatabaseHandler

    @Before
    @After
    fun cleanRepositories() {
        resourceResourceRepository.deleteAll()
    }

//    @Test
//    fun `test getByIds`() = runBlocking {
//        val body: List<Long> = listOf()
//        resourceResourceDatabaseHandler.getByIds(body)
//        Unit
//    }
//
//    @Test
//    fun `test removeAll`() = runBlocking {
//        resourceResourceDatabaseHandler.removeAll()
//        Unit
//    }
//
//    @Test
//    fun `test getAll`() = runBlocking {
//        val maxResults: Int = 100
//        val page: Int = 0
//        resourceResourceDatabaseHandler.getAll(maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test create`() = runBlocking {
//        val body: ResourceResource = ResourceResource()
//        resourceResourceDatabaseHandler.create(body)
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        resourceResourceDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        resourceResourceDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: ResourceResource = ResourceResource()
//        val id: Long = 0
//        resourceResourceDatabaseHandler.update(body, id)
//        Unit
//    }
}
